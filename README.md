# Public Python Projects

## Description
This is a repository that will contain multiple different commits, which will all be programming projects invloving Python. Most, if not all, of these projects will be non-GUI.

## Installation
You must have python installed on your system in order to run these programs.

## Usage
The Psuedoconverter can be user to convert Python code into pseudocode. Might be easier to build a working program in Python, then use the pseudocode generated as a layout for the code to be implemented into another OOP language.

## Roadmap
Most of these projects are simplistic, but I may upload more advanced projects down the line.

## Contributing
Since these are my personal projects, there have been no other contributors to them.

## Authors and acknowledgment
Syed Aby

## License
These projects are personal and taking these without persmission would be very disingenuos of whoever does so.

## Project status
Will get updated as I complete more of my incomplete projects.
